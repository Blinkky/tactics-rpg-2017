﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FacingIndicator : MonoBehaviour
{
    [SerializeField]
    Renderer[] directions;
    [SerializeField]
    Material normal;
    [SerializeField]
    Material selected;

    public void SetDirection(HexOrientation dir)
    {
        int index = (int)dir;
        for (int i = 0; i < 6; ++i)
        {
            directions[i].material = (i == index) ? selected : normal;
        }
    }

    public void SetDirection(int dir)
    {
        if (dir < 0 || dir >= 6)
            return;

        SetDirection((HexOrientation)dir);
    }
}
