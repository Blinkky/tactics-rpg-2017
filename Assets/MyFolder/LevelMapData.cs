﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelMapData : ScriptableObject
{
    public List<TileData> TilePositionList;
}
