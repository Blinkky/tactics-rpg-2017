﻿using UnityEngine;

[System.Serializable]
public struct Point
{
    public int q;
    public int r;

    public Point(int q, int r)
    {
        this.q = q;
        this.r = r;
    }

    public Vector3 GetCubePoint()
    {
        return new Vector3(q * 1.5f, 0, (r + q * 0.5f) * Mathf.Sqrt(3));
    }
}

public enum HexOrientation
{
    Up,
    UpRight,
    DownRight,
    Down,
    DownLeft,
    UpLeft,
}