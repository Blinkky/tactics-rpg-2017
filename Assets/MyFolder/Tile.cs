﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Tile : MonoBehaviour
{
    public Transform RooTransform;

    public Point HexPosition;

    private int m_Height;
    public int Height
    {
        get { return m_Height; }
        set
        {
            m_Height = value;
            UpdateTileState();
        }
    }

    ///// <summary>
    ///// Indicates if something is occupying the cell.
    ///// </summary>
    //public bool IsTaken;

    ///// <summary>
    ///// Cost of moving through the cell.
    ///// </summary>
    //public int MovementCost;

    protected virtual void OnMouseEnter()
    {
        Debug.Log("OnMouseEnter");
        //if (CellHighlighted != null)
        //    CellHighlighted.Invoke(this, new EventArgs());
    }
    protected virtual void OnMouseExit()
    {
        Debug.Log("OnMouseExit");
        //if (CellDehighlighted != null)
        //    CellDehighlighted.Invoke(this, new EventArgs());
    }
    void OnMouseDown()
    {
        Debug.Log("OnMouseDown");
        //if (CellClicked != null)
        //    CellClicked.Invoke(this, new EventArgs());
    }

    public void InitPosition(Point p)
    {
        RooTransform.localPosition = p.GetCubePoint();
    }

    private void UpdateTileState()
    {
        RooTransform.localPosition = HexPosition.GetCubePoint();

        RooTransform.localPosition += new Vector3(0, Height, 0);
    }
}
