﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(LevelEditor))]
public class LevelEditorInspector : Editor
{
    public LevelEditor current
    {
        get
        {
            return (LevelEditor)target;
        }
    }

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        if (GUILayout.Button("Clear"))
            current.Clear();
        if (GUILayout.Button("Create"))
            current.OnCreateButtonClick();
        if (GUILayout.Button("Remove"))
            current.OnRemoveButtonClick();
        //if (GUILayout.Button("Grow Area"))
        //    current.GrowArea();
        //if (GUILayout.Button("Shrink Area"))
        //    current.ShrinkArea();
        if (GUILayout.Button("Save"))
            current.Save();
        if (GUILayout.Button("Load"))
            current.Load();

        if (GUI.changed)
            current.UpdateMarker();
    }
}
