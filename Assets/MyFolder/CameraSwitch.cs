﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSwitch : MonoBehaviour
{
    public Camera[] Cameras;

    private int m_CurCameraIndex;

    public int CurCameraIndex
    {
        get { return m_CurCameraIndex; }
        set
        {
            m_CurCameraIndex = value < Cameras.Length ? value : 0;
            UpdateCameraState();
        }
    }

    // Use this for initialization
    void Start()
    {
        CurCameraIndex = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            CurCameraIndex++;
        }
    }

    private void UpdateCameraState()
    {
        if (Cameras != null && Cameras.Length > 0)
        {
            for (int i = 0; i < Cameras.Length; i++)
            {
                Cameras[i].gameObject.SetActive(i == CurCameraIndex);
            }
        }
    }
}
