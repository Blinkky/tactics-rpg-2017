﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

public class LevelEditor : MonoBehaviour
{
    public GameObject TileGameObject;
    public GameObject TileSelectionIndicatorGameObject;
    private Transform m_Indicator;

    public string LevelName = "Level";

    public float TileSize = 1f;

    public Point HexPosition;

    public LevelMapData MyLevelMapData;

    public LineRenderer LineAxisQ;
    public LineRenderer LineAxisR;

    private Dictionary<Point, Tile> m_Tiles = new Dictionary<Point, Tile>();

    // Use this for initialization
    void Start()
    {
        m_Indicator = Instantiate(TileSelectionIndicatorGameObject).transform;

        LineAxisQ.SetPositions(new[] { Vector3.zero, new Point(10, 0).GetCubePoint() });
        LineAxisR.SetPositions(new[] { Vector3.zero, new Point(0, 10).GetCubePoint() });
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Horizontal"))
        {
            HexPosition.q += Input.GetAxis("Horizontal") > 0 ? 1 : -1;
            UpdateMarker();
        }

        if (Input.GetButtonDown("Vertical"))
        {
            HexPosition.r += Input.GetAxis("Vertical") > 0 ? 1 : -1;
            UpdateMarker();
        }

        if (Input.GetButtonDown("Fire1"))
        {
            CreateSingleTile(HexPosition);
            UpdateMarker();
        }

        if (Input.GetButtonDown("Fire2"))
        {
            RemoveSingleTile(HexPosition);
            UpdateMarker();
        }

        if (Input.GetAxis("Mouse ScrollWheel") > 0)
        {
            PullUpTile(HexPosition);
            UpdateMarker();
        }

        if (Input.GetAxis("Mouse ScrollWheel") < 0)
        {
            PullLowTile(HexPosition);
            UpdateMarker();
        }
    }

    #region Editor

    public void OnCreateButtonClick()
    {
        CreateSingleTile(HexPosition);
    }

    public void OnRemoveButtonClick()
    {
        RemoveSingleTile(HexPosition);
    }

    public void Clear()
    {
        for (int i = transform.childCount - 1; i >= 0; --i)
            DestroyImmediate(transform.GetChild(i).gameObject);
        m_Tiles.Clear();
    }

    public void Save()
    {
        string filePath = Application.dataPath + "/Resources/Levels";
        if (!Directory.Exists(filePath))
            CreateSaveDirectory();

        LevelMapData map = ScriptableObject.CreateInstance<LevelMapData>();
        map.TilePositionList = new List<TileData>(m_Tiles.Count);
        foreach (Point p in m_Tiles.Keys)
        {
            TileData tile = new TileData(p, 0);
            map.TilePositionList.Add(tile);
        }

        string fileName = string.Format("Assets/Resources/Levels/{0}.asset", LevelName);
        AssetDatabase.CreateAsset(map, fileName);
    }

    void CreateSaveDirectory()
    {
        string filePath = Application.dataPath + "/Resources";
        if (!Directory.Exists(filePath))
            AssetDatabase.CreateFolder("Assets", "Resources");
        filePath += "/Levels";
        if (!Directory.Exists(filePath))
            AssetDatabase.CreateFolder("Assets/Resources", "Levels");
        AssetDatabase.Refresh();
    }

    public void Load()
    {
        Clear();
        if (MyLevelMapData == null)
            return;

        foreach (TileData tileData in MyLevelMapData.TilePositionList)
        {
            Point p = tileData.HexPosition;
            int h = tileData.Height;

            if (m_Tiles.ContainsKey(p))
            {
                Debug.LogError(string.Format("Point ({0},{1}) already exsit.", p.q, p.r));
                return;
            }

            Tile t = Create();
            t.InitPosition(p);
            t.RooTransform.name = string.Format("HexTile ({0},{1})", p.q, p.r);
            m_Tiles.Add(p, t);
        }
    }

    private void CreateSingleTile(Point p)
    {
        if (!m_Tiles.ContainsKey(p))
        {
            Tile t = Create();
            t.HexPosition = p;
            m_Tiles.Add(p, t);
        }
    }

    private void RemoveSingleTile(Point p)
    {
        if (m_Tiles.ContainsKey(p))
        {
            Tile t = m_Tiles[p];
            m_Tiles.Remove(p);
            DestroyImmediate(t.RooTransform.gameObject);
        }
    }

    private void PullUpTile(Point p)
    {
        if (m_Tiles.ContainsKey(p))
        {
            Tile t = m_Tiles[p];
            t.Height += 1;
        }
    }

    private void PullLowTile(Point p)
    {
        if (m_Tiles.ContainsKey(p))
        {
            Tile t = m_Tiles[p];
            t.Height -= 1;
        }
    }

    private void TileHeightBackToCenter(Point p)
    {
        if (m_Tiles.ContainsKey(p))
        {
            Tile t = m_Tiles[p];
            t.Height = 0;
        }
    }

    private Tile Create()
    {
        GameObject instance = Instantiate(TileGameObject);
        instance.transform.parent = transform;

        Tile t = instance.GetComponentInChildren<Tile>();
        if (t)
            t.InitPosition(HexPosition);

        return t;
    }

    public void UpdateMarker()
    {
        if (!Application.isPlaying)
            return;

        Point p = HexPosition;
        //float vectorX = TileSize * HexPosition.q * 1.5f;
        //float vectorZ = TileSize * Mathf.Sqrt(3) * (HexPosition.r + HexPosition.q * 0.5f);

        //m_Indicator.localPosition = new Vector3(vectorX, 0, vectorZ);
        m_Indicator.localPosition = p.GetCubePoint() * TileSize;

        if (m_Tiles.ContainsKey(p))
        {
            Tile t = m_Tiles[p];
            m_Indicator.localPosition += new Vector3(0, t.Height, 0);
        }
    }

    //private void OnDrawGizmos()
    //{
    //    Gizmos.DrawLine(Vector3.zero, new Point(0, 5).GetCubePoint());
    //    Gizmos.DrawLine(Vector3.zero, new Point(5, 0).GetCubePoint());
    //}

    #endregion
}
