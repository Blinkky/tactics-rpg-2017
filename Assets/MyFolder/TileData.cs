﻿[System.Serializable]
public struct TileData
{
    public Point HexPosition;

    public int Height;

    public TileData(Point hexPosition, int height)
    {
        this.HexPosition = hexPosition;
        this.Height = height;
    }
}
